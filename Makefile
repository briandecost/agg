# h5c++ --std=c++11 -Wall -O3 agg.cpp -o agg

CC=gcc
CXX=h5c++
RM=rm -f
CPPFLAGS=-g --std=c++11 -Wall -O3
LDFLAGS=-g
LDLIBS=

# SRCS=src/agg.cpp src/dream3d.cpp src/opts.cpp src/grainboundary.cpp
SRCS=$(wildcard src/*.cpp)
OBJS=$(subst .cpp,.o,$(SRCS))

all: agg

agg: $(OBJS)
	$(CXX) $(LDFLAGS) -o agg $(OBJS) $(LDLIBS)

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean-all: clean
	$(RM) agg

clean:
	$(RM) $(OBJS)

dist-clean: clean
	$(RM) *~ .depend

-include .depend

