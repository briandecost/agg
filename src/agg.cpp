#include "agg.h"
#include "dream3d.h"

struct agg_parameters params;

double* gaussianmask(int radius, double sigma, double a, int rank) {
  int masksize = 1;
  int edge_len = (2*radius) + 1;
  for (int i = 0; i < rank; i++) {
    masksize *= edge_len;
  }

  double* mask = new double[masksize];
  double weight = 0.0;
  int x = 0; int y = 0;
  for (int idx = 0; idx < edge_len; idx++) {
    x = idx - radius;
    for (int idy = 0; idy < edge_len; idy++) {
      y = idy - radius;
      weight =  a * exp(-((x*x) + (y*y)) / (2*sigma*sigma));
      if (weight < CUTOFF) {
        weight = 0;
      }
      mask[idy + idx*edge_len] = weight;
    }
  }
  
  return mask;
}

/* enforce periodic boundary conditions */
inline int wrapindex(int pos, int dim) {
  if (pos < 0)
    pos += dim;
  else if (pos >= dim)
    pos -= dim;
  return pos;
}

void uniqueneighbors(std::vector<int> &neighs, int* grains, int site, hsize_t* dims, int radius) {
  int current_grain = grains[site];
  int x = 0; int y = 0;
  int grain = 0;
  int site_x = 0; int site_y = 0;
  site_y = site % dims[0];
  site_x = static_cast<int>(floor(site / dims[0]));

  // precompute y values to lighten the inner loop
  double ylist[FOOTPRINT] = {0};
  for (int idy = 0; idy < FOOTPRINT; idy++) {
    y = site_y-radius+idy;
    ylist[idy] = wrapindex(y, dims[1]);
  }

  for (int idx = site_x-radius; idx <= site_x+radius; idx++) {
    x = wrapindex(idx, dims[0]);
    for (int idy = 0; idy < FOOTPRINT; idy++) {
      y = ylist[idy];
      grain = grains[y + x*dims[0]];
      if (grain != current_grain) {
        if (std::find(neighs.begin(), neighs.end(), grain) == neighs.end()) {
          neighs.push_back(grain);
        }
      }
    }
  }
}

double energy(int* grains, hsize_t* dims, int site, double* mask, int radius) {
  double E = 0.0;
  int grain = grains[site];
  int x = 0; int y = 0;
  int site_x = 0; int site_y = 0;
  
  site_y = site % dims[0];
  site_x = static_cast<int>(floor(site / dims[0]));
  int ctr = 0;

  // precompute y values to lighten the inner loop
  double ylist[FOOTPRINT] = {0};
  for (int idy = 0; idy < FOOTPRINT; idy++) {
    y = site_y-radius+idy;
    ylist[idy] = wrapindex(y, dims[1]);
  }
  
  for (int idx = site_x-radius; idx <= site_x+radius; idx++) {
    x = wrapindex(idx, dims[0]);
    for (int idy = 0; idy < FOOTPRINT; idy++) {
      y = ylist[idy];
      E += mask[ctr] * (grains[y+(x*dims[0])] != grain);
      ctr += 1;
    }
  }
  return E;
}


double site_propensity(int* grains, hsize_t* dims,
                      double* mask, int radius, int site, double kT) {
  int current_state = grains[site];
  double E = energy(grains, dims, site, mask, radius);
  std::vector<int> neighs;
  // more efficient to build a weighted map here?
  uniqueneighbors(neighs, grains, site, dims, 1); // updates neighs

  if (neighs.size() == 0) { return 0.0;}
  
  double prob = 0.0;
  for (auto neigh: neighs) {
    grains[site] = neigh;
    double E_change = energy(grains, dims, site, mask, radius) - E;
    prob += std::min(1.0, exp(-E_change / kT));
  }
  
  grains[site] = current_state;
  double epsilon = 1e-6;
  if (prob <= epsilon) prob = 0.0;
  return prob;
}

void kmc_event(int* grains, double* propensity, hsize_t* dims,
               double* mask, int radius, int site, double kT, double threshold) {
  // int current_state = grains[site];
  double E = energy(grains, dims, site, mask, radius);
  
  std::vector<int> neighs;
  // more efficient to build a weighted map here?
  uniqueneighbors(neighs, grains, site, dims, 1); // updates neighs
  
  double prob = 0.0;
  for (auto neigh: neighs) {
    grains[site] = neigh;
    double E_change = energy(grains, dims, site, mask, radius) - E;
    prob += std::min(1.0, exp(-E_change / kT));
    if (prob > threshold) {
      break;
    }
  }

  // update neighbor propensities
  int x = 0; int y = 0;
  int site_x = 0; int site_y = 0;; int neigh_site = 0;
  site_y = site % dims[0];
  site_x = static_cast<int>(floor(site / dims[0]));
  
  int ctr = 0;
  for (int idx = site_x-radius; idx <= site_x+radius; idx++) {
    x = wrapindex(idx, dims[0]);
    for (int idy = site_y-radius; idy <= site_y+radius; idy++) {
      if (mask[ctr] > 0.0) {
        y = wrapindex(idy, dims[1]);
        neigh_site = y + x*dims[0];
        propensity[neigh_site] = 
          site_propensity(grains, dims, mask, radius, neigh_site, kT);
      }
      ctr++;
    }
  }
  return;
}

// void all_propensity(double* propensity, int sz) {
//   for (int site = 0; site < sz; site++) 
//  propensity[site] = site_propensity(propensity, sz, site);
// }


int selectsite(double* propensity, int sz, double& timestep,
                std::default_random_engine &prng,
                std::uniform_real_distribution<double> &uniform) {
  double totalprop = 0;
  for (int site = 0; site < sz; site++) {
    totalprop += propensity[site];
  }
  
  // modify &timestep (in the caller's scope)
  timestep = - log(uniform(prng)) / totalprop;
  
  double prob = 0;
  double threshold = uniform(prng) * totalprop;
  for (int site = 0; site < sz; site++) {
    prob += propensity[site];
    if (prob > threshold) {
      return site;
    }
  } 
  return 0;
}

int main(int argc, char* argv[]) {
  parse_command_line(argc, argv);
  
  std::default_random_engine prng;
  std::uniform_real_distribution<double> uniform(0.0,1.0);

  int rank = 0; 
  hsize_t* dims;
  int* grain_ids = NULL;
  grain_ids = read_grain_ids(params.infile, rank, dims);
  
  int sz = 1;
  std::cout << "system size: ";
  for (int i = 0; i < rank; i++) {
    std::cout << dims[i];
    sz *= dims[i];
    if (i < rank-1) std::cout << " x ";
  }
  std::cout << std::endl;
  
  // int sz = dataset_size(rank, dims);
  std::cout << "size: " << sz << std::endl;  
  std::cout << "rank: " << rank << std::endl;
  
  int radius = 9;
  double sigma = 3.0;
  double a = 1.0;
  double* mask = gaussianmask(radius, sigma, a, rank);
  
  double* propensity = new double[sz];
  // all_propensity(propensity, sz);
  std::cout << "computing initial propensity values..." << std::endl;
  for (int site = 0; site < sz; site++) {
    propensity[site] = 0;
    propensity[site] = site_propensity(grain_ids, dims, mask, radius, site, params.kT);
  }

  double minprop = 0.0, maxprop = 0.0, totalprop = 0.0;
  for (int site = 0; site < sz; site++) {
    double p = propensity[site];
    minprop = std::min(minprop, p);
    maxprop = std::max(maxprop, p);
    totalprop += p;
  }
  std::cout << "minprop: " << minprop << std::endl;
  std::cout << "maxprop: " << maxprop << std::endl;
  std::cout << "totalprop: " << totalprop << std::endl;
  
  double timestep = 0.0;
  double time = 0.0;
  double threshold = 0.0;
  
  int event_site = 0;
  int snapshot_idx = 0;
  double snapshot = 0.0;
  double snapshot_interval = 2.5;

  
  // write_grain_ids(grain_ids, rank, dims, snapshot_idx);
  std::cout <<"time: " << time << " limit: " << params.length << std::endl;
  while (time < params.length) {
    // selectsite modifies timestep
    event_site = selectsite(propensity, sz, timestep, prng, uniform);

    threshold = uniform(prng) * propensity[event_site];
    kmc_event(grain_ids, propensity, dims, mask, radius, event_site, params.kT, threshold);
    time += timestep;
    if (time > snapshot) {
      std::cout << "time: " << time << std::endl;
      snapshot += snapshot_interval;
      snapshot_idx += 1;
      write_grain_ids(grain_ids, rank, dims, snapshot_idx);
    }
  }
  std::cout << "time: " << time << std::endl;
  std::cout << "cleaning up" << std::endl;
  delete[] mask;
  delete[] propensity;
  delete[] grain_ids;
  delete[] dims;
  
  return 0;
}

