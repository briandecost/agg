#ifndef __DREAM3D_H__
#define __DREAM3D_H__

#include "H5Cpp.h"
#include "agg.h"

int* read_grain_ids(char* filename, int& rank, hsize_t*& dims);
void write_grain_ids(int* grain_ids, int& rank, hsize_t*& dims, int file_id);

#endif
