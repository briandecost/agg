#include "dream3d.h"

const H5std_string GRAIN_IDS_PATH( "DataContainers/SyntheticVolume/CellData/FeatureIds" );

int dset_size(int rank, hsize_t* dims) {
  int the_size = 1;
  for (int i = 0; i < rank; i++) {
    the_size *= dims[i];
  }
  return the_size;
}

int* read_grain_ids(char*filename, int& rank, hsize_t*& dims) {
  int* grain_ids = NULL;
  H5::H5File datafile(filename, H5F_ACC_RDONLY);

  H5::DataSet dset = datafile.openDataSet(GRAIN_IDS_PATH);
  H5::DataSpace filespace = dset.getSpace();

  rank = filespace.getSimpleExtentNdims();
  
  dims = new hsize_t[rank];
  rank = filespace.getSimpleExtentDims( dims );
  int sz = 1;
  for (int i = 0; i < rank; i++) {
    sz *= dims[i];
  }

  H5::DataSpace memspace(rank, dims);

  grain_ids = new int[sz];
  dset.read( grain_ids, H5::PredType::NATIVE_INT, memspace, filespace );

  return grain_ids;
}

void write_grain_ids(int* grain_ids, int& rank, hsize_t*& dims, int file_id) {
  // std::ostringstream filename;
  // filename << "dump" << file_id << ".dream3d";
  char filename[128];
  sprintf(filename, "data/dump%04d.dream3d", file_id);
  H5::H5File datafile(filename, H5F_ACC_TRUNC);
  H5::DataSpace dataspace( rank, dims );
  H5::IntType datatype( H5::PredType::NATIVE_INT);
  datatype.setOrder( H5T_ORDER_LE );

  H5::Group group(datafile.createGroup("/DataContainers"));
  H5::Group group2(datafile.createGroup("/DataContainers/SyntheticVolume"));
  H5::Group group3(datafile.createGroup("/DataContainers/SyntheticVolume/CellData"));
  H5::DataSet dataset = datafile.createDataSet( GRAIN_IDS_PATH, datatype, dataspace );
  dataset.write( grain_ids, H5::PredType::NATIVE_INT );

  return;
}
