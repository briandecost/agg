#include <getopt.h>
#include <iostream>
#include <algorithm>

#include "agg.h"

void print_usage() {
  std::cout << "agg -- Another Grain Growth model" << std::endl;
  std::cout << "agg INPUT.DREAM3D -lT" << std::endl;
  std::cout << "---------------------------------" << std::endl;
  std::cout << "INPUT.DREAM3D: initial microstructure file" << std::endl;
  std::cout << "-l             length in KMC timesteps" << std::endl;
  std::cout << "-T             Monte Carlo temperature kT" << std::endl;
  exit(-1);
  return;
}

extern void parse_command_line(int argc, char* argv[]) {

  // first option is a positional argument
  if (argc == 1) {
    print_usage();
  }
    
  std::string infile = argv[1];
  // double check positional argument for help flag
  if (infile.compare("-h") == 0 || infile.compare("--help") == 0) {
    print_usage();
  }
  params.infile = argv[1];
  
  int   opt = 0;
  argv[1] = argv[0];
  argv++;
  argc--;

  // default values
  params.kT = 0.001;
  params.length = 10.0;
  
  while ((opt = getopt (argc, argv, "l:T:h")) != -1) {
    switch(opt) {
    case 'l':
      params.length = std::stof(optarg);
      break;
    case 'T':
      params.kT = std::stof(optarg);
      break;
    case 'h':
    case '?':
      print_usage();
      break;
    }
  }
  return;
}
