#ifndef __AGG_H__
#define __AGG_H__

#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include <random>
#include <vector>
#include <algorithm>

#include "H5Cpp.h"

#define CUTOFF 0.01
#define FOOTPRINT 19

struct agg_parameters {
  char* infile;
  double kT;
  double length;
};

extern struct agg_parameters params;

extern void parse_command_line(int argc, char* argv[]);

#endif
